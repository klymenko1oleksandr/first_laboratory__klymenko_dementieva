package calculator;
import java.util.Scanner;
/**
 *
 * @author sasha
 */
public class Calculator {
    static Scanner scanner = new Scanner(System.in);
 
    public static void main(String[] args) {
        int a = getfun();
        switch (a){
            case 0:
                System.out.println("Всего хорошего!До свидания");
                return;
            case 1:
              calc();
                break;
            case 2:
                Klymenko();
                break;
            case 3:
                Dementieva();
                break;
            
            default:
                System.out.println("Операция не распознана. Повторите ввод.");
    }

}
 
    public static void calc()
    {
        double num1 = getDouble();
        double num2 = getDouble();
        char operation = getOperation();
        double result = calc(num1,num2,operation);
        System.out.println("Результат операции: "+result);
    }
    
     public static void Klymenko()
    {
        int oper = Klymenko1();
        switch (oper){
            case 0:
                  double num1 = getDouble("площадь основания");
                  double num2 = getDouble("высоту");
                  System.out.println("Объем параллелепипеда равен " + num1*num2);
                return;
            case 1:
                  double num3 = getDouble("длину");
                  double num4 = getDouble("ширину");
                  double num5 = getDouble("высоту");
                  System.out.println("Объем параллелепипеда равен " + num3*num4*num5);
                return;
                default:
                System.out.println("Операция не распознана. Повторите ввод.");
      
    }
    }
     public static int Klymenko1(){
         System.out.println("Добро пожаловать в калькулятор по расчету объема параллелепипеда");
        System.out.println("Через какие аргументы вы хотите посчитать объем?");
        System.out.println("0 Площади основания и высоты");
        System.out.println("1 Длины, ширины и высоты(Только для прямоугольного параллелепипеда)");
        
        int oper;
        if(scanner.hasNextInt()){
            oper = scanner.nextInt();
        } else {
            System.out.println("Вы допустили ошибку при вводе функции. Попробуйте еще раз.");
            //рекурсия
            scanner.next();//рекурсия
            oper = Klymenko1();
        }
        return oper;
     }
     
    
     public static int getfun(){
        System.out.println("Добро пожаловать в калькулятор");
        System.out.println("Какую фунуцию вы хотите выполнить?");
        System.out.println("0 Выход из программы"); 
        System.out.println("1 Обычный калькулятор");    
        System.out.println("2 Клименко (Расчет объема параллелепипеда)"); 
        System.out.println("3 Дементьева (Превраврещание объема)");
        System.out.println("Введите функцию:");
        int num;
        if(scanner.hasNextInt()){
            num = scanner.nextInt();
        } else {
            System.out.println("Вы допустили ошибку при выборе функции. Попробуйте еще раз.");
            scanner.next();//рекурсия
            num = getfun();//рекурсия
        }
        return num;
    }
    
   
    public static char getOperation(){
        System.out.println("Введите операцию:");
        char operation;
        if(scanner.hasNext()){
            operation = scanner.next().charAt(0);
        } else {
            System.out.println("Вы допустили ошибку при вводе операции. Попробуйте еще раз.");
            scanner.next();//рекурсия
            operation = getOperation();
        }
        return operation;
    }
 
    public static double calc(double num1, double num2, char operation){
        double result;
        switch (operation){
            case '+':
                result = num1+num2;
                break;
            case '-':
                result = num1-num2;
                break;
            case '*':
                result = num1*num2;
                break;
            case '/':
                result = num1/num2;
                break;
            default:
                System.out.println("Операция не распознана. Повторите ввод.");
                result = calc(num1, num2, getOperation());//рекурсия
        }
        return result;
    }
    public static void Dementieva()
    {
        int oper = Dementieva1();
        switch (oper){
            case 0:
                  double num1 = getDouble("количество литров");
                  System.out.println(num1*1000 + "ml");
                return;
            case 1:
                  double num2 = getDouble("количество литров"); 
                  System.out.println(num2*0.001 + " cubic meters");
                return;
            case 2:
                  double num3 = getDouble("количество миллилитров");  
                  System.out.println(num3*0.000001 + " cubic meters");
                return;
            case 3:
                 double num4 = getDouble("количество миллилитров"); 
                 System.out.println(num4*0.001 + " litres");
            case 4:
                 double num5 = getDouble("количество метров кубических");
                 System.out.println(num5*1000 + " litres");
            case 5:
                 double num6 = getDouble("количество метров кубических");
                 System.out.println(num6*1000000 + " ml");
                default:
                System.out.println("Операция не распознана. Повторите ввод.");
      
    }}
    
     public static int Dementieva1(){
        System.out.println("Добро пожаловать в калькулятор перевода из одной единицы объема в другую");
        System.out.println("Какие единицы объема вы хотите перевести?");
        System.out.println("0 Из литров в миллилитры");
        System.out.println("1 Из литров в метры кубические");
        System.out.println("2 Из миллилитров в метры кубические");
        System.out.println("3 Из миллилитров в литры");
        System.out.println("4 Из метров кубических в литры");
        System.out.println("5 Из метров кубических в миллилитры");
        int oper;
        if(scanner.hasNextInt()){
            oper = scanner.nextInt();
        } else {
            System.out.println("Вы допустили ошибку при вводе функции. Попробуйте еще раз.");
            //рекурсия
            scanner.next();//рекурсия
            oper = Dementieva1();
        }
        return oper;
     }
     public static double getDouble(){
       System.out.println("Введите число:");
        double num;
        if(scanner.hasNextDouble()){
            num = scanner.nextDouble();
        } else {
            System.out.println("Вы допустили ошибку при вводе числа. Попробуйте еще раз.");
            scanner.next();//рекурсия
            num = getDouble();
        }
        return num;
    }
    
    public static double getDouble(String aaa){
       System.out.println("Введите "+aaa+": ");
        double num;
        if(scanner.hasNextDouble()){
            num = scanner.nextDouble();
        } else {
            System.out.println("Вы допустили ошибку при вводе числа. Попробуйте еще раз.");
            scanner.next();//рекурсия
            num = getDouble();
        }
        return num;
    }
     
}